<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeworks_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	function all(){
		return $this->db->get('homeworks');
	}
}
