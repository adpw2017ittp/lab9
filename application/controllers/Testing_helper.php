<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing_helper extends CI_Controller {
	/** ==================
	 * MENGGUNAKAN HELPER 
	 ** ==================
	 * penjelasan singkat helper
	 * 	helper merupakan kumpulan fungsi php yang dibuat dan 
	 * 	dapat dipakai dimana saja dalam project tersebut.
	 * 
	 * file helper terletak di folder application/helpers/*
	 *
	 * lebih lengkapnya
	 * 	buka https://www.codeigniter.com/userguide3/general/helpers.html
	 * 
	 * laod helper di autoload
	 *  - buka application/config/autoload.php
	 *  - cari $autoload['helper']
	 *  - tambahkan helper yang ingin di load
	 *    contoh: 
	 *			kasus ini kita akan meload file 
	 *				application/helpers/helper1_helper.php 
	 *			
	 *				$autoload['helper'] = array('url','helper1');
	 * 			
	 * 
	 * load helper di controller
	 * - buka controller yang ingin diload helpernya
	 * - arahkan pada fungsi yang akan menggunakan helper
	 * 	 -> jika ingin dapat diload di setiap fungsi di controller tersebut, 
	 *			maka load helper di konstruktor class controller tersebut,
	 *				function __construct(){
	 *					// ini cara meload file helper yang terletak pada 
	 *					// application/helpers/helper1_helper.php
	 *					$this->load->helper('helper1');
	 *				}

	 * 	 -> jika hanya ingin di load di fungsi tertentu dalam suatu controller, 
	 *			maka load helper di fungsi controller yang diinginkan
	 *				function tes_helper1(){
	 *					// ini cara meload file helper yang terletak pada 
	 *					// application/helpers/helper1_helper.php
	 *					$this->load->helper('helper1');
	 *				}
	 */
	public function tes_helper1(){
		$this->load->helper('helper1');

		contoh_helper();
	}

	// load global_helper yang dibuat untuk 
	// mengecek nilai suatu variable dengan 
	// fungsi php var_dump() dan print_r()
	public function tes_globalhelper(){
		$this->load->helper('global');

		$data['css']		= 'Ini CSS';
		$data['js']			= 'Ini JS';
		$data['title']	= 'Passing Variable'; 
		$data['nama']		= 'Muhammad Al-Fatih';

		// mencetak variable dengan print_r 
		// yang dikemas dalam fungsi printr_data($data)
		// didalam global_helper (cek application/helpers/global_helper)
		printr_data($data);
		
	}
}
