<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends CI_Controller {

	// example : url_project = http://localhost:40/lab9
	// buka di url_project/testing/passing_variable
	public function index()
	{
		// echo "<h1>Hello World</h1>";
		$this->load->view("view_testing");
	}

	// buka di url_project/testing/passing_variable
	public function passing_variable()
	{
		$data = array(
			'css'=>'Ini CSS', 
			'js'=>'Ini JS', 
			'title'=>'Passing Variable', 
			'nama'=>'Muhammad Al-Fatih', 
		);

		// passing variable dengan array, variable yang dihasilkan adalah nama index array nya.
		$this->load->view("view_passing_variable",$data);
	}

	// buka di url_project/testing/passing_variable2
	public function passing_variable2()
	{
		$data['css']		= 'Ini CSS';
		$data['js']			= 'Ini JS';
		$data['title']	= 'Passing Variable'; 
		$data['nama']		= 'Muhammad Al-Fatih';

		// cara passing variable dengan teknik lain,
		$this->load->view("view_passing_variable",$data);
	}

	// buka di url_project/testing/passing_variable3
	public function passing_variable3()
	{
		// passing variable array()
		// array tanpa mendefinisikan nama index nya, berarti index nya default dari 0,1,.....
		$data['css']		= array(
			base_url().'assets/css/bootstrap.css',
		);
		$data['js']			= array(
			'assets/js/jQuery-2.1.4.min.js',
			'assets/js/bootstrap.js'
		);

		// passing variable biasa
		$data['title']	= 'Passing Variable'; 
		$data['nama']		= 'Muhammad Al-Fatih';
		$data['page']		= 'view_passing_variable2_page';

		// cara passing variable dengan teknik lain,
		$this->load->view("view_passing_variable2",$data);
	}

	// buka di url_project/testing/templating
	public function templating()
	{
		// lihat dari project_path/application/views/
		// set template, page, dan title halaman
		$template 			= 'templates/tpl_default.php';
		$data['page'] 	= 'pages/view_page1.php';
		$data['title']	= 'Passing Variable'; 

		// set css dan js
		$data['css']		= array(
			base_url().'assets/css/bootstrap.css',
		);
		$data['js']			= array(
			'assets/js/jQuery-2.1.4.min.js',
			'assets/js/bootstrap.js'
		);

		// set data yang akan ditampilan ke page
		$data['data'] 	= array(
			'nama'=>'Muhammad Al-Fatih',
			'nim'=>'14102099'
		);
		// cara passing variable dengan teknik lain,
		$this->load->view($template,$data);
	}
	
}




