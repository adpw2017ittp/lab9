<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing_model extends CI_Controller {

	/** =================
	 * MENGGUNAKAN MODEL
	 ** =================
	 * 
	 * penjelasan singkat menggunakan model
	 * 	 model merupakan kumpulan query database yang dikemas dalam fungsi
	 *   agar dapat digunakan dengan mudah 
	 *   dengan cukup memanggil nama fungsi modelnya
	 * 
	 * query database tersebut dapat berupa 
	 *   select, insert, update, maupun delete.
	 * 
	 * file model disimpan dalam folder application/models/*
	 *   berikan nama *_model.php untuk memudahkan mengenal 
	 *   bahwa objek tersebut adalah model
	 * 
	 * cara menggunakan model
	 * - load model di semua controller
	 *   1. buka file application/config/autoload.php
	 * 	 2. cari $autoload['model']
	 * 	 3. tambahkan model yang ingin diload
	 * 	 		contoh : $autoload['model'] = array('global_model');
	 * 
	 * - load file model dalam controller
	 *   -> model digunakan di seluruh fungsi, 
	 *			maka load model di konstruktor controller
	 * 			contoh : 
	 * 			  function __construct(){
	 * 				  $this->load->model('nama_model');
	 *					
	 *					// pemanggilan fungsi dalam model disesuaikan 
	 *					// dengan nama fungsi model tersebut
	 *					// beserta parameter2 yang dibutuhkan.
	 *					$this->nama_model->nama_fungsi_dalam_model($param,$param2);
	 * 			  }
	 * 
	 *   -> model digunakan di fungsi tertentu di controller tersebut, 
	 * 			maka load model di dalam fungsi yang dinginkan 
	 *			dalam controller tersebut
	 * 			contoh : 
	 * 			  function tes_model(){
	 * 				  $this->load->model('nama_model');
	 *					
	 *					// pemanggilan fungsi dalam model disesuaikan 
	 *					// dengan nama fungsi model tersebut
	 *					// beserta parameter2 yang dibutuhkan.
	 *					$this->nama_model->nama_fungsi_dalam_model($param,$param2);
	 * 			  }
	 * 			
	 */

	function __construct(){
		parent::__construct(); // ini memanggil konstruktor induk
		// detail nya silahkan pelajari konsep OOP (Object Oriented Programming)
	}

	function tes_model(){
		$this->load->helper('global');
		$this->load->model('homeworks_model');

		$get_data = $this->homeworks_model->all();
		$data['data_array'] = $get_data->result_array();
		$data['data_object'] = $get_data->result_object();

		vardump_data($data);
	}

	// tes_model2 ini akan mempraktekkan passing data yang didapat dari model
	// kemudian ditampilkan ke dalam view 
	function tes_model2(){
		$this->load->helper('global');
		$this->load->model('homeworks_model');

		$get_data = $this->homeworks_model->all();
		
		// result data dalam bentuk array
		$data['data_array'] 	= $get_data->result_array(); 

		// result data dalam bentuk object
		$data['data_object'] 	= $get_data->result_object();
		
		// set index title agar menjadi variable title setelah passing data ke view
		$data['title'] 				= 'Testing Model';

		$this->load->view('view_load_model',$data);
	}

	// tes_model2 ini akan mempraktekkan passing data yang didapat dari model
	// kemudian ditampilkan ke dalam view dengan implementasi templating
	function tes_model3(){
		$this->load->helper('global');
		$this->load->model('homeworks_model');

		// lihat dari project_path/application/views/
		// set template, page, dan title halaman
		$template 			= 'templates/tpl_default.php';
		$data['page'] 	= 'pages/view_load_model1.php';
		$data['title']	= 'Passing Variable'; 

		// set css dan js
		$data['css']		= array(
			base_url().'assets/css/bootstrap.css',
		);
		$data['js']			= array(
			'assets/js/jQuery-2.1.4.min.js',
			'assets/js/bootstrap.js'
		);

		// set data yang akan ditampilan ke page
		$get_data = $this->homeworks_model->all();
		$data['data_array'] 	= $get_data->result_array();
		$data['data_object']	= $get_data->result_object();

		// cara passing variable dengan teknik lain,
		$this->load->view($template,$data);
	}
}
