<?php defined ( 'BASEPATH' ) OR exit ( 'No direct script access allowed' );
function printr_data($data=''){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}
function printr_data_ex($data=''){
	printr_data($data);
	die();
	exit();
}
function vardump_data($data=''){
	echo '<pre>';
	var_dump($data);
	echo '</pre>';
}
function vardump_data_ex($data=''){
	vardump_data($data);
	die();
	exit();
}
?>
