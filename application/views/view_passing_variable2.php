<!DOCTYPE html>
<html>
<head>
	<!-- menampilkan variable title -->
	<title>
		<?php echo $title;?>
	</title>

	<!-- menampilkan variable array tanpa pendefinisian index -->
	<!-- menampilkan variable array $css -->
	<?php if (isset($css)&&$css!=null) {
		foreach ($css as $row) {
			echo '<link rel="stylesheet" type="text/css" href="'.$row.'">'."\n\t";
		}
	}?>

	<!-- menampilkan variable js array -->
	<?php if (isset($js)&&$js!=null) {
		foreach ($js as $row) {
			echo '<script type="text/javascript" src="'.$row.'""></script>'."\n\t";
		}
	}?>
	
</head>
<body>
		<?php $this->load->view($page);?>
</body>
</html>
