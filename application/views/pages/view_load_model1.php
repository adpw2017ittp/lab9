
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" style="padding-top:1rem">

	    	<div class="card">
	    		<div class="card-body">
			    	<a class="btn btn-primary float-md-right" href="<?= base_url().'index.php/homework/addhomework';?>">Add Homework</a>
						<h2> Look at all the homeworks I have! </h2>
						<table class="table table-striped table-hover bg-white">
							<tr class="bg-primary text-white">
								<th> No </th>
								<th> Course </th>
								<th> Description </th>
								<th> Deadline </th>
							</tr>
							<?php $no = 1;
							foreach ( $data_object as $row ): ?>
							<tr>
								<td> <?= $no++; ?> </td>
								<td> <?= $row -> course ?> </td>
								<td> <?= $row -> description ?> </td>
								<td> <?= $row -> deadline ?> </td>
							</tr>
							<?php endforeach ; ?>
						</table> <!-- end table.table.table-striped.table-hover.bg-white -->
						<p><small>Note : Data menggunakan variable object</small></p>
					</div> <!-- end div.card-body -->
				</div> <!-- end div.card -->

	    	<div class="card" style="margin-top:1rem">
	    		<div class="card-body">
			    	<a class="btn btn-primary float-md-right" href="<?= base_url().'index.php/homework/addhomework';?>">Add Homework</a>
						<h2> Look at all the homeworks I have! </h2>
						<table class="table table-striped table-hover bg-white">
							<tr class="bg-primary text-white">
								<th> No </th>
								<th> Course </th>
								<th> Description </th>
								<th> Deadline </th>
							</tr>
							<?php $no = 1;
							foreach ( $data_array as $row ): ?>
							<tr>
								<td> <?= $no++; ?> </td>
								<td> <?= $row['course'] ?> </td>
								<td> <?= $row['description'] ?> </td>
								<td> <?= $row['deadline'] ?> </td>
							</tr>
							<?php endforeach ; ?>
						</table> <!-- end table.table.table-striped.table-hover.bg-white -->
						<p><small>Note : Data menggunakan variable array</small></p>
					</div> <!-- end div.card-body -->
				</div> <!-- end div.card -->

			</div>
		</div>
