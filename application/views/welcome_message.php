<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}
	.code{
		border: 1px solid #D0D0D0;
		color: #002166;
		background-color: #f9f9f9;
		padding: 6px 8px;
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
	}
	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	ul.ul-code li{
		padding:.4rem;
	}
	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/Welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>

		<h2>Menu Testing</h2>
		<ul class="ul-code">
			<li><a href="<?= base_url().'testing/passing_variable'?>">Passing Variable type 1</a> lihat di <span class="code">application/controller/Testing.php</span> pada <span class="code">function passing_variable()</span></li>
			<li><a href="<?= base_url().'testing/passing_variable2'?>">Passing Variable type 2</a> lihat di <span class="code">application/controller/Testing.php</span> pada <span class="code">function passing_variable2()</span></li>
			<li><a href="<?= base_url().'testing/passing_variable3'?>">Passing Variable type 3</a> lihat di <span class="code">application/controller/Testing.php</span> pada <span class="code">function passing_variable3()</span></li>
			<li><a href="<?= base_url().'testing/templating'?>">Simple Templating</a> lihat di <span class="code">application/controller/Testing.php</span> pada <span class="code">function templating()</span></li>
		</ul>

		<h2>Menu Testing_helper</h2>
		<ul class="ul-code">
			<li><a href="<?= base_url().'testing_helper/tes_helper1'?>">Penggunaan helper1</a> lihat di <span class="code">application/controller/Testing_helper.php</span> pada <span class="code">function tes_helper1()</span></li>
			<li><a href="<?= base_url().'testing_helper/tes_globalhelper'?>">Penggunaan global_helper</a> lihat di <span class="code">application/controller/Testing_helper.php</span> pada <span class="code">function tes_globalhelper()</span></li>
		</ul>
		<p>folder helper ada di <span class="code">application/helpers/</span></p>

		<h2>Menu Testing_model</h2>
		<ul class="ul-code">
			<li><a href="<?= base_url().'testing_model/tes_model'?>">Penggunaan homeworks_model</a> lihat di <span class="code">application/controller/Testing_model.php</span> pada <span class="code">function tes_model()</span></li>
			<li><a href="<?= base_url().'testing_model/tes_model2'?>">Penggunaan data dari model kemudian di passing ke view (no template)</a> lihat di <span class="code">application/controller/Testing_helper.php</span> pada <span class="code">function tes_model2()</span></li>
			<li><a href="<?= base_url().'testing_model/tes_model3'?>">Penggunaan data dari model kemudian di passing ke view (with template)</a> lihat di <span class="code">application/controller/Testing_helper.php</span> pada <span class="code">function tes_model3()</span></li>
		</ul>
		<p>folder model ada di <span class="code">application/model/</span></p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>
