<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- passing data variable title dari index pada array $data -->
	<title><?= $title?></title>
</head>
<body>

	<div class="container">
		<h2> Look at all the homeworks I have!</h2>
		<table>
			<tr>
				<th> No </th>
				<th> Course </th>
				<th> Description </th>
				<th> Deadline </th>
			</tr>
			<?php $no = 1;
			foreach ( $data_object as $row ): ?>
			<tr>
				<td> <?= $no++; ?> </td>
				<td> <?= $row -> course ?> </td>
				<td> <?= $row -> description ?> </td>
				<td> <?= $row -> deadline ?> </td>
			</tr>
			<?php endforeach ; ?>
		</table> <!-- end table -->
		<p><small>Note : Data menggunakan variable object</small></p>
	</div>

	<div class="container" style="margin-top:5rem">
		<h2> Look at all the homeworks I have!</h2>
		<table>
			<tr>
				<th> No </th>
				<th> Course </th>
				<th> Description </th>
				<th> Deadline </th>
			</tr>
			<?php $no = 1;
			foreach ( $data_array as $row ): ?>
			<tr>
				<td> <?= $no++; ?> </td>
				<td> <?= $row['course'] ?> </td>
				<td> <?= $row['description'] ?> </td>
				<td> <?= $row['deadline'] ?> </td>
			</tr>
			<?php endforeach ; ?>
		</table> <!-- end table -->
		<p><small>Note : Data menggunakan variable array</small></p>
	</div>

</body>
</html>
