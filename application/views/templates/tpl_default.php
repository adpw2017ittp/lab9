<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<!-- load title -->
	<title><?= $title;?></title>

	<!-- load css -->
	<?php if (isset($css)&&$css!=null) {
		foreach($css as $row){
			echo '<link rel="stylesheet" type="text/css" href="'.$row.'">'."\r\n";
		}
	}
	?>

	<!-- menampilkan variable js array -->
	<?php if (isset($js)&&$js!=null) {
		foreach ($js as $row) {
			echo '<script type="text/javascript" src="'.$row.'""></script>'."\n\t";
		}
	}?>
	
	<!-- custom css style -->
	<style type="text/css">
		html, body {
		  height: 100%;
		}

		#wrap {
		  min-height: 100%;
		}

		#main {
		  overflow:auto;
		  padding-bottom:150px; /* this needs to be bigger than footer height*/
		}

		.footer {
		  position: relative;
		  margin-top: -150px; /* negative value of footer height */
		  height: 150px;
		  clear:both;
		  padding-top:20px;
		} 
	</style>
</head>
<body>
	<div id="wrap">
	  <div id="main" class="container-fluid clear-top">
			<?php $this->load->view($page);?>
	  </div>
	</div>

	<footer class="footer text-center">
		<h4>Footer</h4>
		<p>Tampilan ini dibuat dengan framework bootstrap 4 beta<br>
		dengan php framework CodeIgniter-3.1.6</p>
		<p>Page rendered in {elapsed_time} seconds.</p>
	</footer>
</body>
</html>
