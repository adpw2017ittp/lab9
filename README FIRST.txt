setting awal sebelum menggunakan ini,
1. import database yang telah disiapkan 
   dengan nama database yang akan dipakai yaitu "lab9"
   (tanpa pakai petik)

2. periksa application/config/config.php
   - cari $config['base_url'] 
   - sesuaikan value nya dengan project path kalian
     contoh : 
        $config['base_url'] = 'http://localhost:40/lab9/';

3. periksa application/config/database.php
   - cari 
	'username' => 'root',
	'password' => 'lulusskripsi',
	'database' => 'lab9',
     sesuaikan value nya dengan settingan database kalian

     jika user root pada mysql kalian tanpa password, maka 
	'username' => 'root',
	'password' => '',
	'database' => 'lab9',

4. lanjutkan percobaan, silahkan baca tulisan dibawah ini.
sample penggunaan codeigniter 
silahkan dicoba akses controller testing dan panggil di dalam url pada browser.
1. controller Testing.php
2. controller Testing_helper.php
3. controller Testing_model.php

didalam controller tersebut telah disiapkan contoh baris code dalam menggunakan codeigniter.
contoh yang disiapkan yaitu 
- load view
- passing variable ke view
- cara templating sederhana
- cara menggunakan helper
- cara menggunakan model

pelajari apa yang dituliskan dalam komentar baris program.
apabila terjadi error, silahkan dibaca pesan error nya apa, kemudian benarkan kesalahannya pada error yang dimaksudkan dalam pesan error tersebut.

sample ini berguna untuk belajar secara cepat dalam menggunakan codeigniter dan tentunya dapat digunakan sebagai contoh dalam mengerjakan tugas yang ada pada modul 11 php codeigniter


panduan lengkap dapat dilihat secara offline di dalam project
http://localhost:40/lab9/user_guide

note : sesuaikan dengan project_path kalian, (dalam contoh diatas, project path nya ada dalam http://localhost:40/lab9/)